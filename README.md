# lost-in-translation

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Sign language translator developed in React Redux. Translates words using sign language letters.
[Heroku deployment](https://glacial-hamlet-05535.herokuapp.com/)

## Table of Contents

-   [Install](#install)
-   [Usage](#usage)
-   [Maintainers](#maintainers)
-   [Contributing](#contributing)
-   [License](#license)

## Install

```
npm install
```

## Usage

```
npm start
```

## Component tree

This was the component tree we created and used as a guide for implementing our solution.

![Component tree](./react-component-tree.pdf "Component tree")

## Maintainers

@mikaelb
@Mokleiv

## Contributing

Thanks to Noroff for supplying demo code of how to implement redux with login and session API.

## License

MIT © 2022 Mikael Bjerga, Sondre Mokleiv Nygård
