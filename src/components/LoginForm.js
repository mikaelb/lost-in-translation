import { useDispatch } from "react-redux";
import { loginCheckUsernameAction } from "../store/actions/loginActions";
import { useState } from "react";
import { useSelector } from "react-redux";

const LoginForm = () => {
    const dispatch = useDispatch();
    const { loginError, loginAttempting } = useSelector(
        (state) => state.loginReducer
    );

    const [username, setUsername] = useState();

    const onInputHandler = (event) => {
        // Get the value from input and update state
        setUsername(event.target.value);
    };

    const onFormSubmit = (event) => {
        event.preventDefault(); // Stop page-reload'

        // Checks user before being further dispatched to attempt login
        dispatch(loginCheckUsernameAction(username));
    };

    return (
        <form onSubmit={onFormSubmit}>
            <h1 className="login-header">Login to get started</h1>
            <div>
                <input
                    type="text"
                    id="username"
                    placeholder="Enter your username"
                    className="login-input"
                    onChange={onInputHandler}
                />
            </div>
            <button type="submit" className="login-button">
                Login
            </button>
            {loginAttempting && <p className="login-load">Logging in...</p>}
            {loginError && <p className="login-error">{loginError}</p>}
        </form>
    );
};

export default LoginForm;
