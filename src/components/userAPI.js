const apiURL = "https://react-assignment-api-smn.herokuapp.com/translations";
const apiKey = "760d3b4e-4b21-427b-afce-0b08bb787728";

// Fetch for creating new user and adds an empty translation list
export const LoginAPI = {
    async login(username) {
        const response = await fetch(apiURL, {
            method: "post",
            headers: {
                "X-API-Key": apiKey,
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                username: username,
                translations: [],
            }),
        });

        if (!response.ok) {
            const { error = "An unknown error occured" } =
                await response.json();
            throw new Error(error);
        }
        return await response.json();
    },
};

// Fetch for updating translations list, returns updated user object
export const PatchTranslations = {
    async patch(userId, translations) {
        const response = await fetch(`${apiURL}/${userId}`, {
            method: "PATCH", // NB: Set method to PATCH
            headers: {
                "X-API-Key": apiKey,
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                // Provide new translations to add to userId
                translations: translations,
            }),
        });

        if (!response.ok) {
            const { error = "An unknown error occured" } =
                await response.json();
            throw new Error(error);
        }
        return await response.json();
    },
};

// Fetch for returning a specific username
export const CheckUsername = {
    async check(username) {
        const response = await fetch(`${apiURL}?username=${username}`);

        if (!response.ok) {
            const { error = "An unknown error occured" } =
                await response.json();
            throw new Error(error);
        }
        return await response.json();
    },
};
