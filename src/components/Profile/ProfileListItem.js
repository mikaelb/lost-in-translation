import React from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { translateDeleteAction } from "../../store/actions/translationAction";

const ProfileListItem = () => {
    const { id, translations } = useSelector((state) => state.sessionReducer);

    const dispatch = useDispatch();

    // Button handler that sets all translations from current user to deleted = true
    // Does not actually remove from database
    const onDeleteHandler = () => {
        dispatch(translateDeleteAction({ id, translations }));
    };

    // Only get the 10 latest entries
    const translationCutList = translations.slice(-10);

    // List of all translations
    const translationList = translationCutList.map((translation, index) => {
        // Only shows if the translation.delete value = true
        if (!translation.deleted) {
            return (
                <div key={index}>
                    <li className="translation-list-item">{translation.translation}</li>
                </div>
            );
        }
        return "";
    });

    return (
        <>
            <ul className="translation-list">{translationList}</ul>
            <button onClick={onDeleteHandler} className="logout-button">Delete</button>
        </>
    );
};

export default ProfileListItem;