import React from "react";
import ProfileListItem from "./ProfileListItem";
import { useDispatch } from "react-redux";
import { sessionLogoutAction } from "../../store/actions/sessionActions";

const ProfileList = () => {
    const dispatch = useDispatch();

    // Clears the local storage and return the user back to login-page
    const onLogoutHandler = () => {
        dispatch(sessionLogoutAction());
    };

    return (
        <div className="list-container">
            <ProfileListItem />
            <button onClick={onLogoutHandler} className="logout-button">
                Logout
            </button>
        </div>
    );
};

export default ProfileList;