import { useSelector } from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import HandSignal from "./HandSignal";

const HandSignals = () => {
    const { translateWords } = useSelector((state) => state.translationReducer);

    // Checks if each letter is a character and passes letter down in lower case ones
    const signals = [...translateWords].map((char, index) => {
        if (char !== " " && /[a-zA-Z]/.test(char)) {
            return (
                <HandSignal
                    char={char.toLowerCase()}
                    key={index}
                    className="flex-shrink-1"
                />
            );
        }
        return "";
    });

    return (
        <AppContainer>
            <div className="shadow-sm p-3 mb-5 bg-white rounded">
                <div className="d-flex flex-row flex-wrap">
                    {/* Shows empty box if there is no words to translate */}
                    {translateWords !== "" ? signals : <p></p>}
                </div>
            </div>
        </AppContainer>
    );
};

export default HandSignals;
