const HandSignal = (props) => {
    const { char } = props;

    // Imports hand sign image and adds it to a img tag
    const charImage = require(`../../assets/individial_signs/${char}.png`);

    return (
        <div>
            <img src={charImage} alt={char} />
        </div>
    );
};

export default HandSignal;
