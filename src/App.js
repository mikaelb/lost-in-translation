import Translation from "./views/Translation";
import LoginPage from "./views/LoginPage";
import ProfilePage from "./views/ProfilePage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navbar from "./views/Navbar";
import AppContainer from "./hoc/AppContainer";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <AppContainer>
                    <Routes>
                        <Route path="/" element={<LoginPage />} />
                        <Route path="/translation" element={<Translation />} />
                        <Route path="/profile" element={<ProfilePage />} />
                    </Routes>
                </AppContainer>
            </div>
        </BrowserRouter>
    );
}

export default App;
