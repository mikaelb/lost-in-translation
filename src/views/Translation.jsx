import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import {
    translatePatchAction,
    translateSetAction,
} from "../store/actions/translationAction";
import HandSignals from "../components/Translation/HandSignals";

const Translation = () => {
    const dispatch = useDispatch();

    // Accessing necessary state for this component
    const [wordsToTranslate, setWordsToTranslate] = useState();
    const { id, translations, loggedIn } = useSelector(
        (state) => state.sessionReducer
    );

    const onInputChange = (event) => {
        setWordsToTranslate(event.target.value);
    };

    // Sends word to be translated to be translated before
    // updating list of translations and dispatch
    // userId and translations to be patched to API
    const onFormSubmit = (event) => {
        event.preventDefault();
        dispatch(translateSetAction(wordsToTranslate));
        translations.push({
            translation: wordsToTranslate,
            deleted: false,
        });
        dispatch(
            translatePatchAction({
                id,
                translations,
            })
        );
    };

    return (
        <>
            {/* Check if user is logged in, if not redirects back to login */}
            {!loggedIn && <Navigate to="/" />}
            {loggedIn && (
                <>
                    <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
                        <div className="input-group mb-3">
                            <input
                                type="text"
                                id="translateWords"
                                placeholder="Enter the word(s) you want translated"
                                className="form-control"
                                onChange={onInputChange}
                            />
                            <button
                                className="btn btn-outline-secondary"
                                type="submit"
                                id="button-addon2"
                            >
                                Translate
                            </button>
                        </div>
                    </form>
                    <HandSignals />
                </>
            )}
        </>
    );
};

export default Translation;
