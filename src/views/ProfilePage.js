import React from "react";
import "./ProfilePage.css";
import ProfileList from "../components/Profile/ProfileList";
import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

const ProfilePage = () => {
    const { loggedIn } = useSelector((state) => state.sessionReducer);
    // Check to see if the user is already logged in or not
    return (
        <>
            {!loggedIn && <Navigate to="/" />}
            {loggedIn && (
                <>
                    <h1 className="profile-header">Profile</h1>
                    <h2>Translation List: </h2>
                    <ProfileList />
                </>
            )}
        </>
    );
};

export default ProfilePage;
