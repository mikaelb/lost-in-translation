import "./LoginPage.css";
import LoginForm from "../components/LoginForm";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

const LoginPage = () => {
    const { loggedIn } = useSelector((state) => state.sessionReducer);
    // Check to see if the user is already logged in or not
    return (
        <>
            {loggedIn && <Navigate to="/translation" />}
            {!loggedIn && (
                <div className="login-page-container">
                    <LoginForm />
                </div>
            )}
        </>
    );
};

export default LoginPage;