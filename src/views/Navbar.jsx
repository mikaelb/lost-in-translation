import "./Navbar.css";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import logo from "../assets/Logo.png";
import splash from "../assets/Splash.svg";

const Navbar = () => {
    // Checks if user is logged in and gets users username
    const { username, loggedIn } = useSelector((state) => state.sessionReducer);

    return (
        <nav
            className="navbar sticky-top border-bottom border-2 px-5"
            style={{ backgroundColor: "#FFC75F" }}
        >
            <div className="container-fluid">
                <Link to="/translation" className="navbar-brand navFont">
                    <picture className="d-inline-block align-text-top logo-holder">
                        <img
                            src={splash}
                            alt="Splash"
                            width={"75px"}
                            className="splash-image"
                        />
                        <img
                            src={logo}
                            alt="Logo"
                            width={"60px"}
                            className="logo-image"
                        />
                    </picture>
                    Lost in Translation
                </Link>
                {/* Shows profile link if user is logged ins */}
                {loggedIn && (
                    <Link
                        to="/profile"
                        className="d-flex align-items-center profile-link"
                    >
                        {username}
                        <span
                            className="material-icons"
                            style={{ fontSize: "50px" }}
                        >
                            account_circle
                        </span>
                    </Link>
                )}
            </div>
        </nav>
    );
};

export default Navbar;
