// Gives all children bootstrap container style
const AppContainer = ({ children }) => {
    return <div className="container">{children}</div>;
};

export default AppContainer;
