import { CheckUsername, LoginAPI } from "../../components/userAPI";
import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_SUCCESS,
    ACTION_CHECK_USERNAME,
    loginAttemptAction,
    loginErrorAction,
    loginSuccessAction,
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionActions";

export const loginMiddleware =
    (
        { dispatch } // Use dispatch to execute new actions
    ) =>
    (next) =>
    (action) => {
        next(action);

        if (action.type === ACTION_CHECK_USERNAME) {
            // Check if username exists.
            CheckUsername.check(action.payload)
                .then((profiles) => {
                    if (profiles.length === 0) {
                        // Attempts login
                        dispatch(loginAttemptAction(action.payload));
                    } else {
                        // Logs in to existing user
                        dispatch(loginSuccessAction(profiles[0]));
                    }
                })
                .catch((error) => {
                    // Error
                    dispatch(loginErrorAction(error.message));
                });
        }
        if (action.type === ACTION_LOGIN_ATTEMPTING) {
            // Make an http request to try and login.
            LoginAPI.login(action.payload)
                .then((profile) => {
                    // Login Success
                    dispatch(loginSuccessAction(profile));
                })
                .catch((error) => {
                    // Error
                    dispatch(loginErrorAction(error.message));
                });
        }
        if (action.type === ACTION_LOGIN_SUCCESS) {
            // Login success
            dispatch(sessionSetAction(action.payload));
        }
    };
