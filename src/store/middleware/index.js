import { applyMiddleware } from "redux";
import { loginMiddleware } from "./loginMiddleware";
import { sessionMiddleware } from "./sessionMiddleware";
import { translationMiddleWare } from "./translationMiddleware";

export default applyMiddleware(
    loginMiddleware,
    translationMiddleWare,
    sessionMiddleware
);
