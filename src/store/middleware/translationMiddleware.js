import { PatchTranslations } from "../../components/userAPI";
import {
    ACTION_TRANSLATION_DELETE,
    ACTION_TRANSLATION_PATCH,
    translateErrorAction,
} from "../actions/translationAction";
import { sessionPatchAction } from "../actions/sessionActions";

export const translationMiddleWare =
    ({ dispatch }) =>
    (next) =>
    (action) => {
        next(action);

        if (action.type === ACTION_TRANSLATION_PATCH) {
            // Sends user id and translation list to API patch request
            PatchTranslations.patch(
                action.payload.id,
                action.payload.translations
            )
                .then((updatedUser) => {
                    // Returns updated user and dispatches it to session store
                    dispatch(sessionPatchAction(updatedUser));
                })
                .catch((error) => {
                    // Returns error message
                    dispatch(translateErrorAction(error));
                });
        }

        if (action.type === ACTION_TRANSLATION_DELETE) {
            // Changes deleted value to true before sending user id and translation list to
            // API patch request
            let deletedTranslations = action.payload.translations.map(
                (translation) => ({
                    ...translation,
                    deleted: true,
                })
            );

            PatchTranslations.patch(action.payload.id, deletedTranslations)
                .then((updatedUser) => {
                    // Returns updated user and dispatches it to session store
                    dispatch(sessionPatchAction(updatedUser));
                })
                .catch((error) => {
                    // Returns error message
                    dispatch(translateErrorAction(error));
                });
        }
    };
