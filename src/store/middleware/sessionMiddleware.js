import {
    ACTION_SESSION_INIT,
    ACTION_SESSION_LOGOUT,
    ACTION_SESSION_PATCH,
    ACTION_SESSION_SET,
    sessionSetAction,
} from "../actions/sessionActions";

export const sessionMiddleware =
    ({ dispatch }) =>
    (next) =>
    (action) => {
        next(action);

        if (action.type === ACTION_SESSION_INIT) {
            // Checks if a session is stored in local storage and
            // sets session store to previous session
            const storedSession = localStorage.getItem("rlit-ss");

            if (!storedSession) {
                return;
            }

            const session = JSON.parse(storedSession);
            dispatch(sessionSetAction(session));
        }

        if (action.type === ACTION_SESSION_SET) {
            // Sets user session in local storage
            localStorage.setItem("rlit-ss", JSON.stringify(action.payload));
        }

        if (action.type === ACTION_SESSION_PATCH) {
            // Updates local storage session
            localStorage.setItem("rlit-ss", JSON.stringify(action.payload));
        }

        if (action.type === ACTION_SESSION_LOGOUT) {
            // Clears all local storage
            localStorage.clear();
        }
    };
