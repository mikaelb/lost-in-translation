export const ACTION_TRANSLATION_WORDS_SET = "[translation] SET";
export const ACTION_TRANSLATION_PATCH = "[translation] PATCH";
export const ACTION_TRANSLATION_DELETE = "[translation] DELETE";
export const ACTION_TRANSLATION_ERROR = "[translation] ERROR";

export const translateSetAction = (translateWords) => ({
    type: ACTION_TRANSLATION_WORDS_SET,
    payload: translateWords,
});

export const translatePatchAction = (translation) => ({
    type: ACTION_TRANSLATION_PATCH,
    payload: translation,
});

export const translateDeleteAction = (translation) => ({
    type: ACTION_TRANSLATION_DELETE,
    payload: translation,
});

export const translateErrorAction = (error) => ({
    type: ACTION_TRANSLATION_ERROR,
    payload: error,
});
