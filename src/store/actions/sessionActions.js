export const ACTION_SESSION_SET = "[session] SET";
export const ACTION_SESSION_INIT = "[session] INIT";
export const ACTION_SESSION_ERROR = "[session] ERROR";
export const ACTION_SESSION_PATCH = "[session] PATCH";
export const ACTION_SESSION_LOGOUT = "[session] LOGOUT";

export const sessionSetAction = (profile) => ({
    type: ACTION_SESSION_SET,
    payload: profile,
});

export const sessionInitAction = () => ({
    type: ACTION_SESSION_INIT,
});

export const sessionLogoutAction = () => ({
    type: ACTION_SESSION_LOGOUT,
});

export const sessionPatchAction = (translation) => ({
    type: ACTION_SESSION_PATCH,
    payload: translation,
});

export const sessionErrorAction = (error) => ({
    type: ACTION_SESSION_ERROR,
    payload: error,
});
