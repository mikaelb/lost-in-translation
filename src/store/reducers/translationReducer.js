import {
    ACTION_TRANSLATION_WORDS_SET,
    ACTION_TRANSLATION_ERROR,
    ACTION_TRANSLATION_PATCH,
    ACTION_TRANSLATION_DELETE,
} from "../actions/translationAction";

const initialState = {
    translateWords: "",
    error: "",
};

export const translationReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TRANSLATION_WORDS_SET:
            return {
                // Updates stored word for translation
                ...state,
                translateWords: action.payload,
            };
        case ACTION_TRANSLATION_PATCH:
            return {
                // Resets error message
                ...state,
                error: "",
            };
        case ACTION_TRANSLATION_DELETE:
            return {
                // Resets error message
                ...state,
                error: "",
            };
        case ACTION_TRANSLATION_ERROR:
            // Returns error payload
            return {
                ...state,
                loginError: action.payload,
            };
        default:
            return state;
    }
};
