import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_ERROR,
    ACTION_LOGIN_SUCCESS,
} from "../actions/loginActions";

const initialState = {
    loginAttempting: false,
    loginError: "",
};

export const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_LOGIN_ATTEMPTING:
            // Changes loginAttempting to true during Attempt
            return {
                ...state,
                loginAttempting: true,
                loginError: "",
            };
        case ACTION_LOGIN_SUCCESS:
            // Changes state to initial state
            return {
                ...initialState,
            };
        case ACTION_LOGIN_ERROR:
            // Changes loginAttempting to false and returns error payload
            return {
                ...state,
                loginAttempting: false,
                loginError: action.payload,
            };
        default:
            return state;
    }
};
