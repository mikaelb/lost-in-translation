import {
    ACTION_SESSION_LOGOUT,
    ACTION_SESSION_PATCH,
    ACTION_SESSION_SET,
} from "../actions/sessionActions";

const initialState = {
    id: "",
    username: "",
    translations: [
        {
            translation: "",
            deleted: false,
        },
    ],
    loggedIn: false,
};

export const sessionReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_SESSION_SET:
            // Sets loggedIn to true
            return {
                ...action.payload,
                loggedIn: true,
            };
        case ACTION_SESSION_PATCH:
            // Updates translations list
            return {
                ...state,
                translations: [...action.payload.translations],
            };
        case ACTION_SESSION_LOGOUT:
            // Changes state to initial state
            return {
                initialState,
            };
        default:
            return state;
    }
};
